#include "dungeon_features.hpp"

#include <fstream>
#include <iostream>

feature parse(const char* path)
{
	feature new_feature;
	char buffer[MAX_CHAR + 1];	// MAX_CHAR + 1 to allow for a 1 x (MAX_CHAR + '\n') rule or pattern
	std::ifstream in_file;
	int tran_index;			// Transpose index used for rotations
	
	in_file.open(path);

	if (!in_file)
	{
		std::cerr << "Error: Can't open " << path << std::endl;
	}

	in_file >> new_feature.rows;
	in_file >> new_feature.cols;

	if (new_feature.rows * new_feature.cols > MAX_CHAR)
	{
		std::cerr << "Error: Rule/Pattern exceeds MAX_CHAR" << std::endl;
	}

	// Copy rule
	for (int row = 0; row < new_feature.rows; row++)
	{
		in_file >> buffer;

		// Copy a row from the buffer, stopping short of '\n'
		for (int col = 0; col < new_feature.cols; col++)
		{
			new_feature.rule[ROT_0][row * new_feature.cols + col] = buffer[col];
		}
	}

	// Copy pattern
	for (int row = 0; row < new_feature.rows; row++)
	{
		in_file >> buffer;

		// Copy a row from the buffer, stopping short of '\n'
		for (int col = 0; col < new_feature.cols; col++)
		{
			new_feature.pattern[ROT_0][row * new_feature.cols + col] = buffer[col];
		}
	}
	
	// Rotate rule/pattern 90 degrees clockwise
	tran_index = 0;
	for (int col = 0; col < new_feature.cols; col++)
	{
		for (int row = new_feature.rows - 1; row >= 0; row--)
		{
			new_feature.rule[ROT_90][tran_index] = new_feature.rule[ROT_0][row * new_feature.cols + col];
			new_feature.pattern[ROT_90][tran_index] = new_feature.pattern[ROT_0][row * new_feature.cols + col];
			++tran_index;
		}
	}

	// Rotate rule/pattern 180 degrees clockwise
	tran_index = 0;
	for (int row = new_feature.rows - 1; row >= 0; row--)
	{
		for (int col = 0; col < new_feature.cols; col++)
		{
			new_feature.rule[ROT_180][tran_index] = new_feature.rule[ROT_0][row * new_feature.cols + col];
			new_feature.pattern[ROT_180][tran_index] = new_feature.pattern[ROT_0][row * new_feature.cols + col];
			++tran_index;
		}
	}

	// Rotate rule/pattern 270 degrees clockwise
	tran_index = 0;
	for (int col = new_feature.cols - 1; col >= 0; col--)
	{
		for (int row = 0; row < new_feature.rows; row++)
		{
			new_feature.rule[ROT_270][tran_index] = new_feature.rule[ROT_0][row * new_feature.cols + col];
			new_feature.pattern[ROT_270][tran_index] = new_feature.pattern[ROT_0][row * new_feature.cols + col];
			++tran_index;
		}
	}

	in_file.close();

	return new_feature;
}

void print_feature(feature f)
{
	std::cout << "Rows x Columns: " << f.rows << " x " << f.cols << std::endl;
	
	std::cout << "Rule (0 Degrees):" << std::endl;
	for (int row = 0; row < f.rows; row++)
	{
		std::cout << "\t";
		for (int col = 0; col < f.cols; col++)
		{
			std::cout << f.rule[ROT_0][row * f.cols + col];
		}
		std::cout << std::endl;
	}
	
	std::cout << "Rule (90 Degrees):" << std::endl;
	for (int row = 0; row < f.cols; row++)
	{
		std::cout << "\t";
		for (int col = 0; col < f.rows; col++)
		{
			std::cout << f.rule[ROT_90][row * f.rows + col];
		}
		std::cout << std::endl;
	}
	
	std::cout << "Rule (180 Degrees):" << std::endl;
	for (int row = 0; row < f.rows; row++)
	{
		std::cout << "\t";
		for (int col = 0; col < f.cols; col++)
		{
			std::cout << f.rule[ROT_180][row * f.cols + col];
		}
		std::cout << std::endl;
	}
	
	std::cout << "Rule (270 Degrees):" << std::endl;
	for (int row = 0; row < f.cols; row++)
	{
		std::cout << "\t";
		for (int col = 0; col < f.rows; col++)
		{
			std::cout << f.rule[ROT_270][row * f.rows + col];
		}
		std::cout << std::endl;
	}
	
	std::cout << "Pattern (0 Degrees):" << std::endl;
	for (int row = 0; row < f.rows; row++)
	{
		std::cout << "\t";
		for (int col = 0; col < f.cols; col++)
		{
			std::cout << f.pattern[ROT_0][row * f.cols + col];
		}
		std::cout << std::endl;
	}
	
	std::cout << "Pattern (90 Degrees):" << std::endl;
	for (int row = 0; row < f.cols; row++)
	{
		std::cout << "\t";
		for (int col = 0; col < f.rows; col++)
		{
			std::cout << f.pattern[ROT_90][row * f.rows + col];
		}
		std::cout << std::endl;
	}
	
	std::cout << "Pattern (180 Degrees):" << std::endl;
	for (int row = 0; row < f.rows; row++)
	{
		std::cout << "\t";
		for (int col = 0; col < f.cols; col++)
		{
			std::cout << f.pattern[ROT_180][row * f.cols + col];
		}
		std::cout << std::endl;
	}
	
	std::cout << "Pattern (270 Degrees):" << std::endl;
	for (int row = 0; row < f.cols; row++)
	{
		std::cout << "\t";
		for (int col = 0; col < f.rows; col++)
		{
			std::cout << f.pattern[ROT_270][row * f.rows + col];
		}
		std::cout << std::endl;
	}
}


#define MAX_CHAR 100 // Maximum characters for rules/patterns

enum rotations
{
	ROT_0,
	ROT_90,
	ROT_180,
	ROT_270,
	NUM_ROT	
};

typedef struct feature 
{
	int rows;
	int cols;
	char rule[NUM_ROT][MAX_CHAR];
	char pattern[NUM_ROT][MAX_CHAR];
} feature;

feature parse(const char* path);

void print_feature(feature f);


#define DUNGEON_FLOORS 10
#define DUNGEON_FLOOR_WIDTH 10
#define DUNGEON_FLOOR_HEIGHT 10

void build_dungeon(char dungeon[]);

void print_dungeon(char dungeon[]);

char get_dungeon_char(char dungeon[], int floor, int row, int col);

int set_dungeon_char(char dungeon[], int floor, int row, int col, char set_char);


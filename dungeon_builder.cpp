#include <iostream>

#include "dungeon_builder.hpp"
#include "dungeon_features.hpp"

void build_dungeon(char dungeon[])
{
	for (int floor = 0; floor < DUNGEON_FLOORS; floor++)
	{
		for (int row = 0; row < DUNGEON_FLOOR_WIDTH; row++)
		{
			for (int col = 0; col < DUNGEON_FLOOR_HEIGHT; col++)
			{
				 if (set_dungeon_char(dungeon, floor, row, col, '#'))
				 {
					std::cerr << "Error: Failed to initialize dungeon" << std::endl;
				 }
			}
		}
	}
}

void print_dungeon(char dungeon[])
{
	char dungeon_char;

	for (int floor = 0; floor < DUNGEON_FLOORS; floor++)
	{
		std::cout << "Floor " << floor << ":" << std::endl;
		for (int row = 0; row < DUNGEON_FLOOR_WIDTH; row++)
		{
			std::cout << "\t";
			for (int col = 0; col < DUNGEON_FLOOR_HEIGHT; col++)
			{
				if ((dungeon_char = get_dungeon_char(dungeon, floor, row, col)))
				{
					std::cout << dungeon_char;
				}
				else
				{
					std::cerr << "Error: Failed to print dungeon";
				}
			}
			std::cout << std::endl;
		}
	}
}

char get_dungeon_char(char dungeon[], int floor, int row, int col)
{
	if (floor >= DUNGEON_FLOORS || row >= DUNGEON_FLOOR_HEIGHT || col >= DUNGEON_FLOOR_WIDTH)
	{
		std::cerr << "Error: Floor " << floor << ", row " << row << ", column " << col << " is invalid" << std::endl;

		return '\0';
	}

	return dungeon[floor * DUNGEON_FLOOR_WIDTH * DUNGEON_FLOOR_HEIGHT + row * DUNGEON_FLOOR_WIDTH + col];
}

int set_dungeon_char(char dungeon[], int floor, int row, int col, char set_char)
{
	if (floor >= DUNGEON_FLOORS || row >= DUNGEON_FLOOR_HEIGHT || col >= DUNGEON_FLOOR_WIDTH)
	{
		std::cerr << "Error: Floor " << floor << ", row " << row << ", column " << col << " is invalid" << std::endl;
		
		return 1;
	}
	
	dungeon[floor * DUNGEON_FLOOR_WIDTH * DUNGEON_FLOOR_HEIGHT + row * DUNGEON_FLOOR_WIDTH + col] = set_char;
	
	return 0;
}


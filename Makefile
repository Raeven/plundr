# *****************************************************
# Variables to control Makefile operation

CXX = g++
CXXFLAGS = -Wall -g

# ****************************************************
# Targets needed to bring the executable up to date

plundr: main.o dungeon_features.o dungeon_builder.o
	$(CXX) $(CXXFLAGS) -o plundr main.o dungeon_features.o dungeon_builder.o

main.o: main.cpp dungeon_builder.hpp
	$(CXX) $(CXXFLAGS) -c main.cpp

dungeon_features.o: dungeon_features.hpp

dungeon_builder.o: dungeon_builder.hpp dungeon_features.hpp

